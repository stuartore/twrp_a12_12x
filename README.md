# TWRP A12
下载[`TWRP-A12.zip`](https://gitlab.com/stuartore/twrp_a12_12x/-/blob/master/TWRP-A12.zip?ref_type=heads)
>此TWRP仅用于刷机，刷机前请找到可以刷入并成功启动的包，否则会不能启动到系统

### Windows
依次点击`第一步.bat`,`第二步.bat`

### Linux
```
bash Xiaomi_12X_boot_TWRP.sh
```
